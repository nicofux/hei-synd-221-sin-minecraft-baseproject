# Project Electrical Age

This is a project that took place as part of the Lab 231_SIn course. The aim of this project is to create a framework which can be applied to different areas. In this case the framework was applied to a Minecraft mod called Electrical Age. The purpose of the framework is the communication with a field (in this case Minecraft EA) to exchange data. The Framework was realized with the aid of six different components called: Core, Field & Modbus, Database, Web and Smart Controller. The data of the field can be displayed on a website in real time and can be viewed in graphical representation on a website called Grafana. Changes to the data can be made trough a website and the smart controller.

# Core Component

The core component is a digital twin of the field process. The twin is a mirror of the current fieldparameter. The twin is there to interact with the Database, the web and the Smart control.

To create the digital twin each field output and field input is written in a DataPoint. Each DataPoint consists of a value, a label and if it is an output or not. 

# Field & Modbus

The field component consists of register which connect the datapoints and the field. The fieldregister itself doesn't save the values. 

Periodically the register reads the different field and writes the values into the datapoints.The registers are also there to write new values into the field from the Smartcontrol or the Web.

The communication between the field and the program is realized with a modbusconnection (Modbus TCP). In this case the server of the connection is the Minecraft world and the client is the javaprogram.

# Database

The database component is there to save the values of the field. The data is saved on an influx database (hosted by our school) which can be viewed graphically on grafana (grafana.sdi.hevs.ch/). The aim is to also enable the managment of multiple users at the same time. The data is stored in form of a table. 

In this application of the framework ten minutes in the minecraft world equals one day on earth. The starting time of our simulation is 3 days before the current date at 7 o'clock. The complete simulation of our minecraft word takes 25 minutes.  

# Web

The web component is there to interact with the field in form of a website. The website enables the display and modification of certain data in real time. The only thing that is transferred between the website and the program is the label and the value. In summary, the web browser acts as a human-machine interface for an operator: It displays the current status and enters the operator's commands.

# Smart Control

The goal is to calculate the best target values for intelligent and optimized field management.
Key points of the optimization process:
- If the batteries are empty or full, the control will not work properly.
- Let the factory produce as much as possible. 

# Jar file

The jar file is located in: https://gitlab.com/nicofux/hei-synd-221-sin-minecraft-baseproject/-/tree/master/out/artifacts/Minecraft_jar and is called Minecraft.jar.
To use the file you have to open cmd and go into the directory where the file is localted. Example: 

cd C:\Dev\MinecraftProjekt\out\artifacts\Minecraft_jar 

To start the program you can type the following line: 

java -jar Minecraft.jar http://influx.sdi.hevs.ch:8080 SIn07 localhost 1502

Whereas the Syntax is the following: 

java -jar "InfluxDB Server" "Group Name" "ModbusTCP Server" "modbus TCP port" "-modbus4j" (last one is optional)

# javadoc
You can find the javadoc in the following link:

https://gitlab.com/nicofux/hei-synd-221-sin-minecraft-baseproject/-/tree/master/javadoc/hei

