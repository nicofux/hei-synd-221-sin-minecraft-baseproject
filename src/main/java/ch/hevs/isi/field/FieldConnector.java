package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import java.util.Timer;
import java.util.TimerTask;
    /**
    * Class FieldConnector (singleton pattern)
    */
public class FieldConnector implements DataPointListener {
    private static FieldConnector instance = null;
    private FieldConnector(){}
    /**
     * The static method getInstance() returns a reference to the class FieldConnector.
     * It creates the single FieldConnector object if it does not exist.
     */
    public static FieldConnector getInstance(){
        if (instance == null){
            instance = new FieldConnector();
        }
        return instance;
    }
    /**
     * Pushes the label and the value to the field
     * @param label The label of the Datapoint
     * @param value Value of the Datapoint
     */
    private void pushToField(String label, String value){
        System.out.println("FieldConnector: Label: "+ label + ", Value: "+ value);
    }

    /**
     * Gets label and value to push it to the field
     * @param dp Corresponding DataPoint to get label and value
     */
    @Override
    public void onNewValue(DataPoint dp) {
        pushToField(dp.getLabel(), dp.toString());
        ModbusRegister mr = ModbusRegister.getRegisterFromDataPoint(dp);
        mr.write();
    }
    /**
     * Creates a timer witch is updating the inputs every period ms
     * @param period The time to wait to poll the inputs
     */
    public void Timer(int period){
        Timer t1 = new Timer();
        t1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ModbusRegister.poll();
                SmartControl.poll();
            }
        },2000,period);
    }
    //Testing poll() method
    public static void main(String[] args) {
        BooleanRegister b1 = new BooleanRegister("SOLAR_CONNECT_ST",false,609);
        BooleanRegister b2 = new BooleanRegister("WIND_CONNECT_ST",false,613);
        FieldConnector.getInstance().Timer(5000);
    }
}
