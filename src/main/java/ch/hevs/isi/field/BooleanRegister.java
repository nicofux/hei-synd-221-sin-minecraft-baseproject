package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
/**
 * Class for boolean register
 */
public class BooleanRegister extends ModbusRegister{

    private BooleanDataPoint dataPoint;
    /**
     * Constructor
     * @param label The label of the DataPoint
     * @param isOutput If the DataPoint is an Output or an Input
     * @param address The address of the DataPoint
     */
    public BooleanRegister(String label, boolean isOutput, int address) {
        this.dataPoint = new BooleanDataPoint(label, isOutput);
        setRegister(dataPoint, address);
    }
    /**
     * Method read
     * Reads the inputs and updates the value in the corresponding DataPoint
     */
    public void read(){
        try {
            dataPoint.setValue(ModbusAccessor.getInstance().readBoolean(address));
        } catch (ModbusTransportException | ErrorResponseException e) {
            System.out.println("Error BooleanRegister read()");
        }
    }
    /**
     * Method write
     * Writes the current value of the corresponding DataPoint in Modbus
     */
    public void write(){
        try {
            ModbusAccessor.getInstance().writeBoolean(address, dataPoint.getValue());
        } catch (ModbusTransportException | ErrorResponseException e) {
            System.out.println("Error BooleanRegister write()");
        }
    }
}
