package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;
import java.util.TimerTask;
/**
 * Class SmartControl
 */
public class SmartControl {
    /**
     * Constructor of the class which creates instance of Datapoints we want to change and adapt
     */
    public SmartControl() {

    FloatDataPoint BATT_CHRG_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
    FloatDataPoint REMOTE_COAL_SP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
    FloatDataPoint REMOTE_FACTORY_SP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");

    Timer timer = new Timer();
    long period = 2500;
    timer.scheduleAtFixedRate(new TimerTask() {
        @Override
        public void run() {
            if (BATT_CHRG_FLOAT.getValue()>0.95f){
                REMOTE_FACTORY_SP.setValue(1f);
                REMOTE_COAL_SP.setValue(0f);
            }
            if(BATT_CHRG_FLOAT.getValue()<0.3f){
                REMOTE_FACTORY_SP.setValue(0f);
                REMOTE_COAL_SP.setValue(1f);
            }
            if (BATT_CHRG_FLOAT.getValue()<0.55f && BATT_CHRG_FLOAT.getValue()>0.45f ){
                REMOTE_FACTORY_SP.setValue(0f);
                REMOTE_COAL_SP.setValue(0f);
            }
        }
    },0,period);
    }
    /**
     * Method poll to make 2 things happening at the same time (thread)
     * Updates the SmartControl every 2.5seconds
     */
    public static void poll(){
        new SmartControl();
    }
}
