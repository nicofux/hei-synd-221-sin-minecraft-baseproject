package ch.hevs.isi.field;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;
/**
 * Class ModbusAccessor
 */
public class ModbusAccessor {

    private static ModbusAccessor instance = null;
    private ModbusMaster master = null;
    /**
     * Creating objects of ModbusFactory and IpParameters
     */
    ModbusFactory mbf = new ModbusFactory();
    IpParameters params = new IpParameters();

    /**
     * Constructor of the class
     * Connects with Local Host and default port
     */
    private ModbusAccessor() {
        this.connect("LocalHost", 1502);
    }
    /**
     *
     * The static method getInstance() returns a reference to the class ModbusAccessor.
     * It creates the single ModbusAccessor object if it does not exist.
     */
    public static ModbusAccessor getInstance() {
        if (instance == null) {
            instance = new ModbusAccessor();
        }
        return instance;
    }
    /**
     * Method to set up a TCP connection
     *
     * @param ipAddress The IP address we want to connect
     * @param port      The port we want to access
     */
    public void connect(String ipAddress, int port) {
        params.setHost(ipAddress);
        params.setPort(port);
        this.master = mbf.createTcpMaster(params, true);
        try {
            master.init();
        } catch (ModbusInitException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * Method to read a boolean value
     *
     * @param register The register we want to read
     * @return If the Holding Register is used
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public boolean readBoolean(int register) throws ModbusTransportException, ErrorResponseException {
        return master.getValue(BaseLocator.coilStatus(1, register));
    }
    /**
     * Method to read a float value
     *
     * @param register The register we want to read
     * @return The value of the holding register
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public float readFloat(int register) throws ModbusTransportException, ErrorResponseException {
        return (float) master.getValue(BaseLocator.holdingRegister(1, register, DataType.FOUR_BYTE_FLOAT));
    }
    /**
     * Method to write a boolean value
     *
     * @param register The register we want to write in
     * @param value    The value we want to write into the register
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public void writeBoolean(int register, boolean value) throws ModbusTransportException, ErrorResponseException {
        master.setValue(BaseLocator.coilStatus(1,register), value);
    }
    /**
     * Method to write a float value
     *
     * @param register The register we want to write in
     * @param value    The value we want to write into the register
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public void writeFloat(int register, float value) throws ModbusTransportException, ErrorResponseException {
        master.setValue(BaseLocator.holdingRegister(1, register, DataType.FOUR_BYTE_FLOAT), value);
    }
    //Testing Modbus component
    public static void main(String[] args) {
        ModbusAccessor test = ModbusAccessor.getInstance();
        test.connect("LocalHost", 1502);
        while (true) {
            try {
                boolean solar = test.readBoolean(609);
                System.out.println("Solar is ON ?" + (solar ? "YES" : "NO"));
            } catch (ModbusTransportException | ErrorResponseException e) {
                //throw new RuntimeException(e);
                // }
                // catch (ErrorResponseException e) {
                // throw new RuntimeException(e);
                // }
                // Utility.waitSomeTime(2000);}
            }
        }
    }
}