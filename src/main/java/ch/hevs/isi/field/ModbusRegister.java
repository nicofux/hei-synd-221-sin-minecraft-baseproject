package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import java.util.HashMap;
/**
 * Class ModbusRegister
 */
public abstract class ModbusRegister {
    protected int address;
    private static final HashMap<DataPoint, ModbusRegister>  modbusRegisterHashMap = new HashMap<>();
    /**
     * Puts a DataPoint with its corresponding register into the HashMap
     * @param address The register of the DataPoint
     * @param dp The DataPoint
     */
    protected void setRegister(DataPoint dp, int address){
        this.address = address;
        modbusRegisterHashMap.put(dp, this);
    }
    /**
     * Gets the register of a DataPint
     * @param dataPoint The DataPoint we want to know the register of
     * @return The corresponding register
     */
    public static ModbusRegister getRegisterFromDataPoint(DataPoint dataPoint){
        return modbusRegisterHashMap.get(dataPoint);
    }
    /**
     * Default implementation of the methods: Here, do nothing
     */
    public abstract void read();
    public abstract void write();
    /**
     * Goes through the HashMap and reads every input of the DataPoints in the HashMap
     */
    public static void poll() {
        for (ModbusRegister fr : modbusRegisterHashMap.values()) {
            fr.read();
        }
    }
}
