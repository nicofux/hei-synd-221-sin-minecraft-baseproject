package ch.hevs.isi.db;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *  Class DatabaseConnector (singleton pattern)
 */
public class DatabaseConnector implements DataPointListener {
    private String protocol_ = null;
    private String hostname_;
    private String bucket_ = "SIn07";
    private String token_;
    private String project_token="andXM-xZ5ZaxLOIyyfShSMMe44wXt-UnM_CX_GablDX9VReyg8dYwSMoQ5xj2vqY4VOJld7aN8RTJha1EZ-t0Q==";
    private static DatabaseConnector instance = null;
    private long _timestamp = 0;
    TimeManager tm;
    /**
     *  private Constructor so that there is only one instance in this specific class
     */
    private DatabaseConnector(){
        tm = new TimeManager(3);
    }
    /**
     *  The static method getInstance() returns a reference to the class DatabaseConnector.
     *  It creates the single DatabaseConnector object if it does not exist.
     */
    public static DatabaseConnector getInstance(){
        if (instance == null){
            instance = new DatabaseConnector();
        }
        return instance;
    }
    /**
     *  Method to initialize the URL of the connection
     *  @param protocol enter specific protocol
     *  @param hostname to acess specific server
     *  @param bucket String with the group number
     *  @param token Password to enter the chosen group
     */
    public void initialize(String protocol, String hostname, String bucket, String token ) {
        protocol_ = protocol;
        hostname_ = hostname;
        bucket_ = bucket;
        token_ = token;
    }

    /**
     *  Pushes the label and the value to the database
     * @param label The label of the Datapoint
     * @param value value of the datapoint
     */
    private void pushToDatabase(String label, String value){
        System.out.println("DataBaseConnector: Label: "+ label + ", Value: "+ value);

        try{
            //Create the URL
            URL url1 = new URL(protocol_ + "://" + hostname_ + "/api/v2/write?org=" + bucket_ + "&bucket=" + bucket_);
            //Create Connection
            HttpURLConnection connection = (HttpURLConnection)  url1.openConnection();
            //Configure authorisation based on HTTP token authorization
            connection.setRequestProperty("Authorization", "Token " + project_token);
            connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("POST");
            //Indicate that there is a message body
            connection.setDoOutput(true);
            //Fetch an OutputStreamWriter object for the HttpURLConnection
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            //Write the body of the HTTP Post request into the OutputStreamWriter
            writer.write(label + " value=" + value + " "+_timestamp);
            writer.flush();
            //Control the responseCode
            int responseCode = connection.getResponseCode();
            if (responseCode!=204){
                System.out.println("Could not write to DataBase, " + responseCode);
            }
            //Close the communication
            writer.close();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     *  Gets label and value to push it to the database
     * @param dp Corresponding DataPoint to get label and value
     */
    @Override
    public void onNewValue(DataPoint dp) {
        if (dp.getLabel().equals("CLOCK_FLOAT")) {
            tm.setTimestamp(dp.toString());
            _timestamp = tm.getNanosForDB();
        }
        if (_timestamp!=0) {
            pushToDatabase(dp.getLabel(), dp.toString());
        }
    }
}
