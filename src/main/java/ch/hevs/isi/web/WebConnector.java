package ch.hevs.isi.web;
import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;

/**
 *  Class WebConnector (singleton pattern)
 */
public class WebConnector implements DataPointListener {

    private static WebConnector instance = null;
    private List<WebSocket> vector = new LinkedList<>();

    private WebConnector(){
        WebSocketServer wss = new WebSocketServer(new InetSocketAddress(8888)) {
            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                webSocket.send("Welcome in the world MineCraft electrical Age");
                vector.add(webSocket);
            }

            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {
                vector.remove(webSocket);
            }

            @Override
            public void onMessage(WebSocket webSocket, String s) {
                if (s != null) {
                    String[] message = s.split("=");
                    if (message.length >= 2) {
                        String label = message[0];
                        String value = message[1];
                        if (value.equals("true") || value.equals("false")) {
                            System.out.println("the variable " + label + "is " + value);
                            BooleanDataPoint bdp = (BooleanDataPoint) DataPoint.getDataPointFromLabel(label);
                            bdp.setValue(Boolean.parseBoolean(value));
                        } else {
                            System.out.println("the variable " + label + "is " + value);
                            FloatDataPoint fdp = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);
                            fdp.setValue(Float.parseFloat(value));
                        }
                    }
                }
            }
            @Override
            public void onError(WebSocket webSocket, Exception e) {
                vector.remove(webSocket);
            }

            @Override
            public void onStart() {
                System.out.println("WebSocketServer started !");
            }
        };
        wss.setConnectionLostTimeout(10000);
        wss.start();
    }
    /**
     *  The static method getInstance() returns a reference to the class WebConnector.
     *  It creates the single WebConnector object if it does not exist.
     */
    public static WebConnector getInstance(){
        if (instance == null){
            instance = new WebConnector();
        }
        return instance;
    }
    /**
     *  Pushes the label and the value to the web
     */
    private void pushToWeb(String label, String value){
    System.out.println("WebConnector: Label: "+ label + ", Value: "+ value);
        for (WebSocket ws : vector) {
            ws.send(label + "=" + value);
        }
    }
    /**
     *  Gets label and value to push it to the web
     * @param dp Corresponding DataPoint to get label and value
     */
    @Override
    public void onNewValue(DataPoint dp) {
        pushToWeb(dp.getLabel(), dp.toString());
    }
}
