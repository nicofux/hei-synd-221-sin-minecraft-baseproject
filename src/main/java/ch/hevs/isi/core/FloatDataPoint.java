package ch.hevs.isi.core;
/**
 * Class for float DataPoint
 */
public class FloatDataPoint extends DataPoint {

    private float value;

    /**
     * Constructor
     *
     * @param label    The label of the DataPoint
     * @param isOutput If the DataPoint is an Output or an Input
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Methode to set value
     * @param value New value to set
     */
    public void setValue(float value) {
        this.value = value;
        updateValue();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
    /**
     * Method to get value
     * @return The value as a String
     */
    public float getValue() {
        return value;

    }
}
