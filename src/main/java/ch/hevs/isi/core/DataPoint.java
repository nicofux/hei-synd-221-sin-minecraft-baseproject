package ch.hevs.isi.core;
import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import java.util.HashMap;
/**
 * Motherclass of the DataPoints
 */
public abstract class DataPoint {

    private static final HashMap<String, DataPoint> dataPointMap = new HashMap<>();
    private String label;
    private Boolean isOutput;

    /**
     * Constructor
     * @param label The label of the DataPoint
     * @param isOutput If the DataPoint is an Output or an Input
     */
    protected DataPoint(String label, boolean isOutput){
    this.label = label;
    this.isOutput =isOutput;
    dataPointMap.put(label, this);
    }
    /**
     * Method to get a specific DataPint from a known label
     * @param label The label of the DataPoint
     * @return Returns the Datapoint from a specific label
     */
    public static DataPoint getDataPointFromLabel (String label){
        return dataPointMap.get(label);
    }
    /**
     * Method to get the label of the DataPoint
     * @return Returns the corresponding label
     */
    public String getLabel(){
        return label;
    }
    /**
     * Method to know if the DatePoint is an output or an input
     * @return Returns true for an output
     */
    public boolean isOutput(){
        return isOutput;
    }
    /**
     *  Default implementation of the methods: Here, do nothing
     */
    public void setValue(boolean value){}
    public void setValue(float value){}
    public abstract String toString();
    /**
     *  Method to update the connectors
     */
    protected void updateValue() {
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        if (isOutput()) {
            FieldConnector.getInstance().onNewValue(this);
        }
    }
    /**
     *  Main to test input and output updating
     */
    public static void main(String[] args) {
    //Create float and boolean input
    FloatDataPoint fdp1 = new FloatDataPoint("Fdp1",false);
    BooleanDataPoint bdp1 = new BooleanDataPoint("Bdp1", false);
    //Create float and boolean output
    FloatDataPoint fdp2 = new FloatDataPoint("Fdp2",true);
    BooleanDataPoint bdp2 = new BooleanDataPoint("Bdp2", true);

    //Initialize the inputs
    System.out.println("Inputs:");
    fdp1.setValue(1.0f);
    bdp1.setValue(true);

    //Update the inputs
    System.out.println("Updated inputs:");
    fdp1.setValue(3.0f);
    bdp1.setValue(false);

    //Initialize the outputs
    System.out.println("Outputs:");
    fdp2.setValue(2.0f);
    bdp2.setValue(true);

    //Update the outputs (only their labels are known)
    System.out.println("Updated outputs:");
    DataPoint dp1 = getDataPointFromLabel("Fdp2");
    dp1.setValue(4.0f);
    DataPoint dp2 = getDataPointFromLabel("Bdp2");
    dp2.setValue(false);
    }
}

