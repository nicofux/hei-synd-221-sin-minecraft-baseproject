package ch.hevs.isi.core;
/**
 *  Interface for the DataPoints
 */
public interface DataPointListener {
    void onNewValue(DataPoint dp);
}
