package ch.hevs.isi.core;
/**
 * Class for boolean DataPoint
 */
public class BooleanDataPoint extends DataPoint{

    private boolean value;
    /**
     * Constructor
     * @param label The label of the DataPoint
     * @param isOutput If the DataPoint is an Output or an Input
     */
    public BooleanDataPoint(String label, boolean isOutput){
        super(label,isOutput);
    }
    /**
     * Methode to set value
     * @param value New value to set
     */
    public void setValue (boolean value){
        this.value = value;
        updateValue();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
    /**
     * Method to get value
     * @return The value as a String
     */
    public boolean getValue(){
        return value;
    }
}
